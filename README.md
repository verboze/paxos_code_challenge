## About:
This repo contains solutions to a three technical challenges
* challenge 1: hash digest API endpoint
* challenge 2: giftcard optimization
* challenge 3: combinations

## Requirements:
* a unix-like environment (tested on macOS 10.2)
* python3.4+ (tested on python3.4.3, python3.6.1)
* install prerequisites: pip3 install -r requirements.txt
              
## Usage:
*challenge 1*:
* run utility: `python3 -m api.tornadoapp logging=debug` (this will start the API server -- ctrl-c to kill it)
* post a message to API endpoint: `curl -i -X POST -H "Content-Type: application/json" -d '{"message": "foo"}' http://volatilekarma.com:8080/messages/`
* retrieve previously posted message if exists: `curl -i http://volatilekarma.com:8080/messages/6a607731ef9e7a12e8b28c9d066178de49427dcccc0b7b76d20435cbb9022693`
NOTE: running this program creates a "hashes.db" sqlite database on your machine in the directory the program is executed. Feel free to remove this after testing

Service bottlenecks: as userbase grows, the data storage will be taxed more heavily. One solution is to distribute the data accross several machines, for example 
by segregating users by region. The goal would be to keep the number of records in each DB manageable so that concurrent queries do not bog down the system.
We would also need to scale out the front end web service, by providing some redundancy in the service and load-balancing the requests that come in

NOTE: post/query order may matter during hash collisionsa, which should be extremely rare.

assumptions:
* Hash calculation/DB storage is currently blocking. a more involved implementation would make it non-blocking via callbacks
* no fault tolerance built-in at the moment


*challenge 2*:
* run utility: `python3 -m giftcard.buygifts giftcard/tests/sampledata.in 25`
    * positional parameter 1: input file
    * positional parameter 2: gift card amount
    * positional parameter 3: (optional) number of friends to get gift for. defaults to 2

Assumptions:
* assuming [item, price] is always separated by a comma
* if multiple combinations are possible, a random one is returned


*challenge 3*:
* run utility: `python3 -m combos.findcombinations x0x1`, where "x0x1" is the input string

Assumptions:
* input and bitmasks used in implementation can fit in memory
* it is assumed input will fit into memory, and that there will be space left for a bitmap array and other minimal data structs used internally
* program will not throw errors if it finds characters other than 0,1,x, and will instead silenty ignore them
* program will throw error on missing input
* input is case-insensitive


## Testing:
run all unittests: from project root directory: `python3 -m unittest discover -v .`

NOTE: for the API tests under api/tests, you may need to open the test port 9000, or update the script to use an open port on your machine
