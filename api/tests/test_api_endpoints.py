#!/usr/bin/env python2.7

import sys
import json
import hashlib
from tornado.testing import AsyncHTTPTestCase
from api.tornadoapp import Application as webapp
sys.path.append('..')


DB = "test.db"


# class TestAPIEndpoint(unittest.TestCase):
class TestAPIEndpoint(AsyncHTTPTestCase):
    '''validate that endpoint comes up'''
    def get_app(self):
        return webapp(DB)

    def test_post(self):
        r = self.fetch('/messages', method='POST', body=json.dumps({'message': 'foo'}))
        self.assertEqual(r.code, 200)
        self.assertEqual(r.body, b'{"digest": "2c26b46b68ffc68ff99b453c1d30413413422d706483bfa0f98a5e886266e7ae"}')

    def test_invalid_post(self):
        r = self.fetch('/messages', method='POST', body='bad json data')
        self.assertEqual(r.code, 200)
        self.assertEqual(r.body, b'{"error": "could not decode json"}')

    def test_get_existing(self):
        qhash = str(hashlib.sha256('bar'.encode('utf-8')).hexdigest())
        self.fetch('/messages', method='POST', body=json.dumps({'message': 'bar'}))
        r = self.fetch('/messages/{}'.format(qhash), method='GET')
        self.assertEqual(r.code, 200)
        self.assertEqual(r.body, b'{"message": "bar"}')

    def test_get_nonexisting(self):
        qhash = 'somerandominvalidhash'
        r = self.fetch('/messages/{}'.format(qhash), method='GET')
        self.assertEqual(r.code, 404)
        self.assertEqual(r.body, b'{"err_msg": "Message not found"}')
