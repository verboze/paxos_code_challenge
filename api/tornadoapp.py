# simple tornado version for an API endpoint

import re
import sys
import json
from tornado.ioloop import IOLoop
import tornado.web
from . import hashing
import logging


class MainHandler(tornado.web.RequestHandler):
    def get(self):
        self.write('Welcome. Post a message to endpoint /messages ' +
                   'to get the SHA256 digest of it. You can also ' +
                   'retrieve an existing message at massages/<hash> :)\n')


class postHandler(tornado.web.RequestHandler):
    def post(self):
        logging.debug("received data: " + str(self.request.body))
        payload = hashing.storemsg(self.request.body)
        self.set_header("Content-Type", "application/json")
        self.write(json.dumps(payload))


class getHandler(tornado.web.RequestHandler):
    def write_errmsg(self):
        self.clear()
        self.set_status(404)
        self.set_header("Content-Type", "application/json")
        self.finish(json.dumps({"err_msg": "Message not found"}))

    def get(self, qhash=None):
        if not qhash or not re.match('^[A-Fa-f0-9]{64}$', qhash):
            self.write_errmsg()
            return

        entry = hashing.readhash(qhash)
        if not entry:
            self.write_errmsg()
            return

        self.set_header("Content-Type", "application/json")
        self.write(json.dumps({"message": entry}))


class Application(tornado.web.Application):
    def __init__(self, db):
        hashing.init(db)
        handlers = [ (r"/?", MainHandler),
                     (r"/messages[/]?", postHandler),
                     (r"/messages/(.*)", getHandler), ]
        tornado.web.Application.__init__(self, handlers)


def main(port=8080, db="hashes.db"):
    # hashing.init(db)
    app = Application(db)
    app.listen(port)
    logging.info('Listening for requests...')
    try:
        IOLoop.instance().start()
    except KeyboardInterrupt:
        IOLoop.instance().stop()


if __name__ == '__main__':
    try:
        loglevel = sys.argv[1].split('=')[1]
        numeric_level = getattr(logging, loglevel.upper(), None)
        if not isinstance(numeric_level, int):
            numeric_level = logging.ERROR
    except IndexError:
        # logging parameter not provided, only log errors
        numeric_level = logging.ERROR

    logging.basicConfig(level=numeric_level, format='%(asctime)s - %(name)s - %(levelname)s - %(message)s')

    # start ioloop listener
    main()
