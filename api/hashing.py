import json
import hashlib
import sqlite3
from lru import LRU
import logging


_conn = None
cachesize = 1000
_recent_hashes = LRU(cachesize, callback=lambda x,y: persist(x, y))  # in-memory cache for fast retrieval
_recent_messages = LRU(cachesize)


def init(dbfile="hashes.db"):
    # I would normally separate DB functions into a different module
    # but for the purposes of this exercies, we will keep things simple
    # by managing everything here...

    # I would also use mongodb or something similar for better
    # async I/O on DB in the real world
    global _conn
    _conn = sqlite3.connect(dbfile)
    _conn.execute('''CREATE TABLE IF NOT EXISTS hashdigests
                     (digest text NOT NULL, msg text NOT NULL,
                     added_on DATETIME DEFAULT CURRENT_TIMESTAMP)''')
    # not allowing for dupe entries via unique key constraint
    try:
        _conn.execute('CREATE UNIQUE INDEX idx_digest ON hashdigests (digest)')
    except sqlite3.OperationalError as e:
        if e.args[0] == 'index idx_digest already exists':
            pass
        else:
            raise


def storemsg(msg):
    try:
        msg = json.loads(msg.decode('utf-8'))['message']
    except Exception as e:
        logging.error(e)
        return {"error": "could not decode json"}

    exists = readmsg(msg)
    if exists:
        logging.debug('found existing hash: {}:{}'.format(exists['digest'], msg))
        return exists

    m = str(hashlib.sha256(msg.encode('utf-8')).hexdigest())
    _recent_hashes[m] = msg
    _recent_messages[msg] = m

    return {"digest": m}


def persist(digest, msg):
    logging.debug('attempting to persist - {}: {}'.format(digest, msg))
    try:
        with _conn:
            _conn.execute("INSERT INTO hashdigests(digest, msg) VALUES (?, ?)", (digest, msg))
    except Exception as e:
        logging.error(e)


def readhash(qhash):
    if qhash not in _recent_hashes:
        cur = _conn.cursor()
        cur.execute("SELECT digest, msg FROM hashdigests WHERE digest=?", (qhash,))
        for row in cur:
            _recent_hashes[row[0]] = row[1]

    return _recent_hashes.get(qhash, None)


def readmsg(msg):
    if msg in _recent_messages:
        return {'digest': _recent_messages[msg]}

    cur = _conn.cursor()
    cur.execute("SELECT digest, msg FROM hashdigests WHERE msg=? LIMIT 1", (msg,))
    for row in cur:
        return {'digest': row[0]}

    return None
