import sys
import unittest
import resource
sys.path.append('..')

from combos.findcombinations import gen_combinations


class TestCombinations(unittest.TestCase):
    '''test combination generator'''

    def store_results(self, val):
        self.results.append(val)

    def setUp(self):
        self.results = []

    def test_no_X(self):
        data = '001'
        gen_combinations(data, self.store_results)
        self.assertEqual(len(self.results), 1)
        self.assertEqual(self.results[0], '001')

    def test_one_X(self):
        data = 'x'
        expected_results = ['0', '1']
        gen_combinations(data, self.store_results)
        self.assertEqual(len(self.results), 2)
        self.assertTrue(all([i in self.results for i in expected_results]))

    def test_some_Xs(self):
        data = 'x1x'
        expected_results = ['010', '011', '110', '111']
        gen_combinations(data, self.store_results)
        self.assertEqual(len(self.results), 4)
        self.assertTrue(all([i in self.results for i in expected_results]))

    def test_lotsof_Xs(self):
        # rudimentary mem limit. the program should not crash
        # TODO: this should be set on a separate process
        # est_sizelimit = sys.getsizeof(bin(2**10)[2:]) * 5 + sys.getsizeof("{}" * 10) + sys.getsizeof(2**10)
        # print("LIMIT:", est_sizelimit)
        # r = resource.RLIMIT_DATA
        # soft, hard = resource.getrlimit(r)
        # resource.setrlimit(r, (est_sizelimit, est_sizelimit))

        # should not crash, even with memory limit
        data = 'xxxxxxxxxx'
        def action(val):
            pass  # does nothing, just a mock action on generated combination
        gen_combinations(data, action)

        # restore default limits
        # resource.setrlimit(r, (soft, hard))
