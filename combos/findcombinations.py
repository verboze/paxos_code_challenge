import sys

# assumption: input and bitmasks used below can fit in memory
# runtime is 2^n, where n is the number of x's in data


def gen_combinations(inputstr, action=print):
    # algo: create a mask string, where we will be replacing {} with 0 or 1
    #       a binary num of length n can represent 2^n unique numbers
    #       we can use this property to therefore count from 0 to 2^n,
    #       where n is the number of x's found in input string. we will
    #       then use our mask string against each number to get our
    #       unique strings. this approach is memory-efficient, and
    #       therefore shouldn't crash on arbitrarely large inputs

    xcount = inputstr.count('x')
    basestr = ''.join(['{}' if i == 'x' else i for i in inputstr])  # formatter string
    num_possibilities = 2**xcount  # number of possible combinations

    for i in range(0, num_possibilities):
        mask = bin(i)[2:].zfill(xcount)
        action(basestr.format(*mask))


if __name__ == "__main__":
    try:
        # inputstr = sys.stdin.readline().strip().lower()
        inputstr = sys.argv[1]
    except IndexError:
        raise Exception('missing paramter...')

    gen_combinations(inputstr)
