import sys
import logging
import itertools
import heapq


class Items():
    def __init__(self, items):
        # note: Items is a list of tuples, expl: [(car: 5000), (bicycle: 100)]
        self.items = items
        self.cost = Items._calc_cost(items)

    def __lt__(self, other):
        return self.cost < other.cost

    def __eq__(self, other):
        left = [set(i) for i in sorted(self.items)]
        right = [set(i) for i in sorted(other.items)]
        if len(left) != len(right):
            return False
        return all([left[i] == right[i] for i in range(len(left))])

    def __len__(self):
        return len(self.items)

    def __repr__(self):
        return str(self.items)

    def __str__(self):
        return ', '.join(['{} {}'.format(*i) for i in self.items])

    @staticmethod
    def _calc_cost(items):
        return sum([i[1] for i in items])


def findmax(card_balance, giftitems, howmany):
    # algo: find the most expenive item (max heap?)
    # if item price > balance: remove item
    #     --> remove all items that cost more than gift card
    # find max sum(item1, item2, itemN) for item1, item2, itemN in combination(items)
    #     --> build custom max heap holding elements of combination(items)
    #     --> elements are (i1, i2 ... iN), compare key is sum(elem)
    # okay, python makes this part easy since I don't have to build the data structs from ground up XD
    h = []
    for i in itertools.combinations(giftitems, howmany):
        # skip combos that are too expensive together
        if Items._calc_cost(i) > card_balance:
            continue
        # build a min heap
        heapq.heappush(h, Items(i))

    logging.debug('HEAP: {}'.format((h)))
    if not h:
        return None

    return h[-1]


def parse_input(argv):
    try:
        infile = argv[1]
        card_balance = int(argv[2])
        howmany = 2
        if len(argv) > 3:
            howmany = int(argv[3])
    except (TypeError, ValueError):
        logging.error('missing paramters/invalid parameters\n')
        sys.exit(1)

    try:
        giftitems = []
        card_balance = int(card_balance)
    except ValueError:
        raise Exception('invalid card balance')

    try:
        # first pass through the data. we elimitate any item that is
        # singly more expensive than my balance, because I'm cheap
        with open(infile, 'r') as f:
            logging.debug('DEBUG - INPUT: [')
            for line in f:
                # create list [(name1: price1), (name2: price2), ... (nameN: priceN)]
                # this will later be used to create Items objects
                tmpitem = line.rsplit(',', 1)
                i, j = tmpitem
                tmpitem = (i.strip(), int(j.strip()))
                logging.debug('    {}'.format(tmpitem))
                if tmpitem[1] >= card_balance and howmany == 1:
                    continue
                giftitems.append(tmpitem)
            logging.debug(']')
    except (FileNotFoundError, ValueError):
        raise Exception('invalid input file')
        sys.exit(1)

    return card_balance, giftitems, howmany


if __name__ == "__main__":
    # setup logging
    for i in sys.argv:
        if i.startswith('--loglevel='):
            loglevel = i.split('=')[1]
            numeric_level = getattr(logging, loglevel.upper(), None)
            if not isinstance(numeric_level, int):
                numeric_level = logging.ERROR
            del(sys.argv[sys.argv.index(i)])
        else:
            # logging parameter not provided, only log errors
            numeric_level = logging.ERROR

    logging.basicConfig(level=numeric_level, format='%(asctime)s - %(name)s - %(levelname)s - %(message)s')

    card_balance, giftitems, howmany = parse_input(sys.argv)
    items = findmax(card_balance, giftitems, howmany)
    if not items:
        print('Not possible')
    else:
        print(items)
