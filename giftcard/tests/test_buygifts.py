import sys
import unittest
sys.path.append('..')

from giftcard.buygifts import Items, parse_input, findmax


class TestGiftcard(unittest.TestCase):
    '''test combination generator'''

    def setUp(self):
        self.numfriends = '2'
        self.balance = '25'
        self.datafile = 'giftcard/tests/sampledata.in'

    def test_find_one_item(self):
        self.numfriends = '1'
        card_balance, giftitems, howmany = parse_input(['_', self.datafile, self.balance, self.numfriends])
        items = findmax(card_balance, giftitems, howmany)
        expected_items = Items([('Earmuffs', 20)])
        self.assertEqual(len(items), 1)
        self.assertEqual(items, expected_items)

    def test_find_two_items(self):
        card_balance, giftitems, howmany = parse_input(['_', self.datafile, self.balance, self.numfriends])
        items = findmax(card_balance, giftitems, howmany)
        expected_items = Items([('Detergent', 10),
                                ('Headphones', 14)])
        self.assertEqual(len(items), 2)
        self.assertEqual(items, expected_items)

    def test_find_three_items(self):
        self.numfriends = '3'
        card_balance, giftitems, howmany = parse_input(['_', self.datafile, self.balance, self.numfriends])
        items = findmax(card_balance, giftitems, howmany)
        expected_items = Items([('Candy Bar', 5),
                                ('Paperback Book', 7),
                                ('Detergent', 10)])
        self.assertEqual(len(items), 3)
        self.assertEqual(items, expected_items)

    def test_find_no_items(self):
        self.balance = 1
        card_balance, giftitems, howmany = parse_input(['_', self.datafile, self.balance, self.numfriends])
        items = findmax(card_balance, giftitems, howmany)
        self.assertTrue(items is None)
